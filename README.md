# PLR2

## Exemple

```
cd existing_repo
git remote add origin https://git.ista.ac.at/ldemaine/plr2.git
git branch -M main
git push -uf origin main
```

## Installation

### Package installation
This package can be used in two different ways:

- First you can just do a symlink in your working file.
  For that you'll have to link the second PLR2 directory (the one with `__init__.py` in it).

  In your working directory, just type the following instruction:
  `ln -s /.../.../PLR2/PLR2`

- The other way is to properly install the package on your system.

  You have to go into the first PLR2 directory.
  Depending of your system, you need to write one of the following line:
  `pip install .`
  or
  `python -m pip install .`

### Usage setup

Once the package is install, you have to create a configuration file named `.PLR2.conf`
in your home directory.
This configuration file tells the package the location where each set of data is stored.
It has the form `NAME = /path/to/data`.

Here's an exemple of my own configuration file on spiritix :
```
RCEmip = /bdd/MT_WORKSPACE/REMY/RCEMIP
TOOCAN = /homedata/roca/RCEMIP/MESONH/TOOCAN/TOOCAN_v2023_05
SELF   = /home/ldemaine/ISTA/DATAS_SELF 
```

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
