#!/usr/bin/env python
# coding: utf-8

# In[1]:


#-*- coding:UTF-8 -*-



### adapted from code of Thomas Fiolleau, in spiritx1:/home/jpitelet/codes_sublime3/RCE_MESOHNH/TOOCAN/exemple_RCE.py

##########################################################################
# Histogrammes permettant de choisir les variables les plus pertinentes  #
# pour ensuite les utiliser dans les methodes de clustering              #
##########################################################################

### Librarys importation

import numpy as np
import gzip

###==============================###
### Class                        ###

class MCS_IntParameters(object):

    def __init__(self):
        self.label       		= 0 # Label of the convective system in the segmented images
        self.qc_MCS	 			= 0 # Quality control of the convective system
        self.duration			= 0 # Duration of the convective system (slot)
        self.Tmax               = 0.
        self.time_Init			= 0 # Time TU of initiation of the convective system
        self.iInit			    = 0 # longitude of the center of mass at inititiation
        self.jInit			    = 0 # Latitude of the center of mass at inititiation
        self.time_End			= 0 # Time TU of dissipation of the convective system
        self.iEnd				= 0 # Longitude of the center of mass at dissipation
        self.jEnd				= 0 # Latitude of the center of mass at dissipation
        self.vavg				= 0 # Average velocity during its life cycle(m/s)
        self.dist				= 0 # Distance covered by the convective system during its life cycle(km)
        self.tbmin				= 0
        self.surfmaxPix_235K	= 0
        self.surfmaxkm2_235K	= 0
        self.surfmaxkm2_220K   	= 0
        self.surfmaxkm2_210K   	= 0
        self.surfmaxkm2_200K   	= 0
        self.surfcumkm2_235K   	= 0
        self.surfcumkm2_220K   	= 0
        self.surfcumkm2_210K   	= 0
        self.surfcumkm2_200K   	= 0
        self.pr_total   		= 0
        self.pr_max      		= 0
        self.surfmaxkm2_00mmh 	= 0
        self.surfmaxkm2_02mmh	= 0
        self.surfmaxkm2_05mmh	= 0
        self.surfmaxkm2_10mmh	= 0

    def var(self,
            exp    = False,
            value  = True,
            indent = 0,
            ):

        print(f'''
MCS_IntParameters [all ints]
├── label: {self.label if value else '':<7}           {'label of the convective system in the segmented images.' if exp else ''}
├── qc_MCS: {self.qc_MCS if value else '':<7}          {'Quality control of the convective system.' if exp else ''}
├── duration: {self.duration if value else '':<7}        {'Durationof the convective system (slot).' if exp else ''}
├── time_Init: {self.time_Init if value else '':<7}       {'Time TU of initiation of the convective system.' if exp else ''}
├── iInit: {self.iInit if value else '':<7}           {'Longitude of the center of mass at inititiation.' if exp else ''}
├── jInit: {self.jInit if value else '':<7}           {'Latitude of the center of mass at inititiation.' if exp else ''}
├── time_End: {self.time_End if value else '':<7}        {'Time TU of dissipation of the convective system.' if exp else ''}
├── iEnd: {self.iEnd if value else '':<7}            {'Longitude of the center of mass at dissipation.' if exp else ''}
├── jEnd: {self.jEnd if value else '':<7}            {'Latitude of the center of mass at dissipation.' if exp else ''}
├── vavg: {self.vavg if value else '':<7}            {'Average velocity during its life cycle(m/s).' if exp else ''}
├── dist: {self.dist if value else '':<7}            {'Distance covered by the convective system during its life cycle(km).' if exp else ''}
├── tbmin: {self.tbmin if value else '':<7}           {'Minimum Brigthness temperature (K).' if exp else ''}
├── pr_total: {self.pr_total if value else '':<7}
├── pr_max: {self.pr_max if value else '':<7}
├── surfmaxPix_235K: {self.surfmaxPix_235K if value else '':<7} {'Maximum surface for a 235K threshold of the convective system during its life cycle (pixel).' if exp else ''}
├── surfmaxkm2_235K: {self.surfmaxkm2_235K if value else '':<7} {'Maximum surface for a 235K threshold of the convective system during its life cycle (km2).' if exp else ''}
├── surfmaxkm2_220K: {self.surfmaxkm2_220K if value else '':<7} {'Maximum surface for a 220K threshold of the convective system during its life cycle (km2).' if exp else ''}
├── surfmaxkm2_210K: {self.surfmaxkm2_210K if value else '':<7} {'Maximum surface for a 210K threshold of the convective system during its life cycle (km2).' if exp else ''}
├── surfmaxkm2_200K: {self.surfmaxkm2_210K if value else '':<7} {'Maximum surface for a 200K threshold of the convective system during its life cycle (km2).' if exp else ''}
├── surfcumkm2_235K: {self.surfcumkm2_235K if value else '':<7} {'Integrated cumulated surface for a 235K threshold of the convective system during its life cycle (km2).' if exp else ''}
├── # surfcumkm2_220K: {self.surfcumkm2_220K if value else '':<5} {'Integrated cumulated surface for a 220K threshold of the convective system during its life cycle (km2).' if exp else ''}
├── # surfcumkm2_210K: {self.surfcumkm2_210K if value else '':<5} {'Integrated cumulated surface for a 210K threshold of the convective system during its life cycle (km2).' if exp else ''}
├── # surfcumkm2_200K: {self.surfcumkm2_200K if value else '':<5} {'Integrated cumulated surface for a 200K threshold of the convective system during its life cycle (km2).' if exp else ''}
├── surfmaxkm2_00mmh: {self.surfmaxkm2_00mmh if value else '':<7} {'Maximum surface for a 00mmh threshold of the convective system during its life cycle (km2).' if exp else ''}
├── surfmaxkm2_02mmh: {self.surfmaxkm2_02mmh if value else '':<7} {'Maximum surface for a 02mmh threshold of the convective system during its life cycle (km2).' if exp else ''}
├── surfmaxkm2_05mmh: {self.surfmaxkm2_05mmh if value else '':<7} {'Maximum surface for a 05mmh threshold of the convective system during its life cycle (km2).' if exp else ''}
├── surfmaxkm2_10mmh: {self.surfmaxkm2_10mmh if value else '':<7} {'Maximum surface for a 10mmh threshold of the convective system during its life cycle (km2).' if exp else ''}
│
└── clusters <MCS_Lifecycle>''')
        self.clusters.var(exp=exp, value = value, indent = indent+1)

class MCS_Lifecycle(object):

    def __init__(self):
        self.tbmin				= []
        self.tbavg_235K			= []
        self.tb_90thPercentile  = []
        self.time				= []
        self.x					= []
        self.y					= []
        self.velocity			= []
        self.semiminor_220K		= []
        self.semimajor_220K		= []
        self.orientation_220K	= []
        self.excentricity_220K	= []
        self.semiminor_235K		= []
        self.semimajor_235K		= []
        self.orientation_235K	= []
        self.excentricity_235K	= []
        self.surf235K_pix		= []
        self.surf210K_pix		= []
        self.surfkm2_235K		= []
        self.surfkm2_220K		= []
        self.surfkm2_210K		= []
        self.surfkm2_200K		= []

        self.pr_mean			= []
        self.pr_max				= []
        self.condpr				= []
        self.surf00mmh_km2		= []
        self.surf01mmh_km2		= []
        self.surf02mmh_km2		= []
        self.surf05mmh_km2		= []
        self.surf10mmh_km2		= []


    def var(self,
            exp      = False,
            value    = True,
            indent   = 0,
            ind_size = 4,
            ):

        print(f'''{' '*ind_size*indent}├── tbmin: {self.tbmin if value else ''} {'min brightness temperature of the convective system at day TU (K)' if exp else ''}
{' '*ind_size*indent}├── tbavg_235K: {self.tbavg_235K if value else ''}  {'average brightness temperature of the convective system at day TU (K) ' if exp else ''}
{' '*ind_size*indent}├── tb_90thPercentile: {self.tb_90thPercentile if value else ''}  {'average brightness temperature of the convective system at day TU (K) ' if exp else ''}
{' '*ind_size*indent}├── time: {self.time if value else ''}  {'day TU ' if exp else ''}
{' '*ind_size*indent}├── x: {self.x if value else ''}  {'column of the center of mass (pixel)' if exp else ''}
{' '*ind_size*indent}├── y: {self.y if value else ''}  {'line of the center of mass(pixel)' if exp else ''}
{' '*ind_size*indent}├── velocity: {self.velocity if value else ''}  {'instantaneous velocity of the center of mass (m/s)' if exp else ''}
{' '*ind_size*indent}├── semiminor_220K: {self.semiminor_220K if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── semimajor_220K: {self.semimajor_220K if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── orientation_220K: {self.orientation_220K if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── excentricity_220K: {self.excentricity_220K if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── semiminor_235K: {self.semiminor_235K if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── semimajor_235K: {self.semimajor_235K if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── orientation_235K: {self.orientation_235K if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── excentricity_235K: {self.excentricity_235K if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── surf235K_pix: {self.surf235K_pix if value else ''}     {'surface of the convective system at time day TU (pixel)' if exp else ''}
{' '*ind_size*indent}├── surf210K_pix: {self.surf210K_pix if value else ''}     {'surface of the convective system at time day TU (pixel)' if exp else ''}
{' '*ind_size*indent}├── surfkm2_235K: {self.surfkm2_235K if value else ''}     {'surface of the convective system for a 235K threshold' if exp else ''}
{' '*ind_size*indent}├── surfkm2_220K: {self.surfkm2_220K if value else ''}     {'surface of the convective system for a 220K threshold' if exp else ''}
{' '*ind_size*indent}├── surfkm2_210K: {self.surfkm2_210K if value else ''}     {'surface of the convective system for a 210K threshold' if exp else ''}
{' '*ind_size*indent}├── surfkm2_200K: {self.surfkm2_200K if value else ''}     {'surface of the convective system for a 200K threshold' if exp else ''}
{' '*ind_size*indent}│
{' '*ind_size*indent}├── pr_mean: {self.pr_mean if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── pr_max: {self.pr_max if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── condpr: {self.condpr if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── surf00mmh_km2: {self.surf00mmh_km2 if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── surf01mmh_km2: {self.surf01mmh_km2 if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── surf02mmh_km2: {self.surf02mmh_km2 if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}├── surf05mmh_km2: {self.surf05mmh_km2 if value else ''}  {'' if exp else ''}
{' '*ind_size*indent}└── surf10mmh_km2: {self.surf10mmh_km2 if value else ''}  {'' if exp else ''}
        ''')


###==============================###
### Fuctions                     ###

def load_TOOCAN(FileTOOCAN):

    lunit=gzip.open(FileTOOCAN,'rt')
    print (FileTOOCAN)
    #
    # Read the Header
    ##########################
    header1  = lunit.readline()
    header2  = lunit.readline()
    header3  = lunit.readline()
    header4  = lunit.readline()
    header5  = lunit.readline()
    header6  = lunit.readline()
    header7  = lunit.readline()
    header8  = lunit.readline()
    header9  = lunit.readline()
    header10 = lunit.readline()
    header11 = lunit.readline()
    header12 = lunit.readline()
    header13 = lunit.readline()
    header14 = lunit.readline()
    header15 = lunit.readline()
    header16 = lunit.readline()
    header17 = lunit.readline()
    header18 = lunit.readline()
    header19 = lunit.readline()
    header20 = lunit.readline()
    header21 = lunit.readline()
    header22 = lunit.readline()
    header23 = lunit.readline()
    data = []
    iMCS = -1
    lines = lunit.readlines()
    for iline in lines:
        Values = iline.split()
        #print (iline,Values)
        if(Values[0] == '==>'):

            #
            # Read the integrated parameters of the convective systems
            ###########################################################
            data.append(MCS_IntParameters())
            iMCS = iMCS+1
            data[iMCS].label 			= int(Values[1])	    # Label of the convective system in the segmented images
            data[iMCS].qc_MCS			= int(Values[2])	    # Quality control of the convective system 
            data[iMCS].duration			= float(Values[3])/2	# duration of the convective system (slot)
            data[iMCS].time_Init		= float(Values[4])		# time TU of initiation of the convective system
            data[iMCS].iInit			= float(Values[5])		# longitude of the center of mass at inititiation
            data[iMCS].jInit			= float(Values[6])		# latitude of the center of mass at inititiation
            data[iMCS].time_End		    = float(Values[7])		# time TU of dissipation of the convective system
            data[iMCS].iEnd				= float(Values[8])		# longitude of the center of mass at dissipation
            data[iMCS].jEnd				= float(Values[9])		# latitude of the center of mass at dissipation
            data[iMCS].vavg				= float(Values[10])		# average velocity during its life cycle(m/s)
            data[iMCS].dist				= float(Values[11])		# distance covered by the convective system during its life cycle(km)
            data[iMCS].tbmin			= float(Values[12])		# minimum Brigthness temperature (K)
            data[iMCS].surfmaxPix_235K	= int(Values[13])		# maximum surface for a 235K threshold of the convective system during its life cycle (pixel)
            data[iMCS].surfmaxkm2_235K	= float(Values[14])		# maximum surfacefor a 235K threshold of the convective system during its life cycle (km2)
            data[iMCS].surfmaxkm2_220K	= float(Values[15])		# maximum surfacefor a 235K threshold of the convective system during its life cycle (km2)
            data[iMCS].surfmaxkm2_210K	= float(Values[16])		# maximum surfacefor a 235K threshold of the convective system during its life cycle (km2)
            data[iMCS].surfmaxkm2_200K	= float(Values[17])		# maximum surfacefor a 235K threshold of the convective system during its life cycle (km2)
            data[iMCS].surfcumkm2_235K	= float(Values[18])
            data[iMCS].pr_total			= float(Values[19])
            data[iMCS].pr_max			= float(Values[20])
            data[iMCS].surfmaxkm2_00mmh	= float(Values[21]) 	# integrated cumulated surface for a 00mmh threshold of the convective system during its life cycle (km2)
            data[iMCS].surfmaxkm2_02mmh	= float(Values[22]) 	# integrated cumulated surface for a 02mmh threshold of the convective system during its life cycle (km2)
            data[iMCS].surfmaxkm2_05mmh	= float(Values[23]) 	# integrated cumulated surface for a 05mmh threshold of the convective system during its life cycle (km2)
            data[iMCS].surfmaxkm2_10mmh	= float(Values[24]) 	# integrated cumulated surface for a 10mmh threshold of the convective system during its life cycle (km2)
            data[iMCS].clusters = MCS_Lifecycle()
            if(data[iMCS].duration < 5):
                data[iMCS].classif = 1

            inc = 0
        else:
            #print Values
            #print Values[21]
            #
            # Read the parameters of the convective systems 
            #along their life cycles
            ##################################################
            data[iMCS].clusters.tbmin.append(float(Values[0]))	    			#min brightness temperature of the convective system at day TU (K)
            data[iMCS].clusters.tbavg_235K.append(float(Values[1]))	    		#average brightness temperature of the convective system at day TU (K) 
            data[iMCS].clusters.tb_90thPercentile.append(float(Values[2]))		#average brightness temperature of the convective system at day TU (K) 
            data[iMCS].clusters.time.append(float(Values[3]))	    			#day TU 
            data[iMCS].clusters.x.append(int(Values[4]))		    			#column of the center of mass (pixel)
            data[iMCS].clusters.y.append(int(Values[5]))		    			#line of the center of mass(pixel)
            data[iMCS].clusters.velocity.append(float(Values[6]))	    		#instantaneous velocity of the center of mass (m/s)

            data[iMCS].clusters.semiminor_235K.append(float(Values[7]))	        #
            data[iMCS].clusters.semimajor_235K.append(float(Values[8]))	        #
            data[iMCS].clusters.excentricity_235K.append(float(Values[9]))  	#
            data[iMCS].clusters.orientation_235K.append(float(Values[10]))   	#
            data[iMCS].clusters.semiminor_220K.append(float(Values[11]))	 	#
            data[iMCS].clusters.semimajor_220K.append(float(Values[12]))	   	#
            data[iMCS].clusters.excentricity_220K.append(float(Values[13]))  	#
            data[iMCS].clusters.orientation_220K.append(float(Values[14]))   	#
            data[iMCS].clusters.surf235K_pix.append(int(Values[15]))	    	#surface of the convective system at time day TU (pixel)
            data[iMCS].clusters.surf210K_pix.append(int(Values[16]))	    	#surface of the convective system at time day TU (pixel)
            data[iMCS].clusters.surfkm2_235K.append(float(Values[17]))  		#surface of the convective system for a 235K threshold
            data[iMCS].clusters.surfkm2_220K.append(float(Values[18]))  		#surface of the convective system for a 200K threshold
            data[iMCS].clusters.surfkm2_210K.append(float(Values[19]))  		#surface of the convective system for a 210K threshold
            data[iMCS].clusters.surfkm2_200K.append(float(Values[20]))  		#surface of the convective system for a 220K threshold
            data[iMCS].clusters.pr_mean.append(float(Values[21]))	    		#surface of the convective system at time day TU (pixel)
            data[iMCS].clusters.pr_max.append(float(Values[22]))	    		#surface of the convective system at time day TU (pixel)
            data[iMCS].clusters.condpr.append(float(Values[23]))  				#surface of the convective system for a 235K threshold
            data[iMCS].clusters.surf00mmh_km2.append(float(Values[24]))  		#surface of the convective system for a 200K threshold
            data[iMCS].clusters.surf01mmh_km2.append(float(Values[25]))  		#surface of the convective system for a 210K threshold
            data[iMCS].clusters.surf02mmh_km2.append(float(Values[26]))  		#surface of the convective system for a 220K threshold
            data[iMCS].clusters.surf05mmh_km2.append(float(Values[27]))  		#surface of the convective system for a 210K threshold
            data[iMCS].clusters.surf10mmh_km2.append(float(Values[28]))  		#surface of the convective system for a 220K threshold


    return data


###=================================###
### Use exemples from Thomas's code ###
# In[4]:

if __name__=='__main__':

    import matplotlib.pyplot as plt
    import matplotlib.ticker as mticker
    from statsmodels.distributions.empirical_distribution import ECDF
    import netCDF4


# In[5]:
    # filetracking = '/bdd/MT_WORKSPACE/MCS/RCE/MESONH/TOOCAN/TOOCAN_v2023_05/Dspread1K/irtb/FileTracking/TOOCAN-MESONH_large300_2D_irtb.dat.gz'
    filetracking = '/home/leo/ISTA/TOOCAN/300K/Dspread1K/irtb/FileTracking/TOOCAN-MESONH_large300_2D_irtb.dat.gz'
    data = load_TOOCAN(filetracking)

    nDCS = len(data)

    LifetimeDuration = [data[iMCS].duration for iMCS in np.arange(0,len(data),1)]
    Smax             = [data[iMCS].surfmaxkm2_235K for iMCS in np.arange(0,len(data),1)]




# In[7]:

    BINS = 2*np.arange(141)

    fig, ax1 = plt.subplots(figsize=(10,8), dpi=120)

    n, bins, patches = ax1.hist(LifetimeDuration, BINS,
                                histtype  = 'step',
                                lw        = 2,
                                color     = 'mediumblue',
                                label     = 'lifetime duration [h]',
                                log       = 1,
                                rwidth    = 15,
                                linewidth = 3,
                                )

    ax1.set_xlabel('Lifetime duration [h]', fontsize=15)
    ax1.set_ylabel('Population', fontsize=15)
    ax1.xaxis.set_major_locator(mticker.MultipleLocator(4))
    ax1.xaxis.set_minor_locator(mticker.MultipleLocator(2))
    ax1.set_xlim(0, 40)
    ax1.grid(True, linewidth=0.5, which='minor', axis='x')
    ax1.grid(True, linewidth=1.5,   which='major', axis='x')
    ax1.grid(True, linewidth=0.5, which='minor', axis='y')
    ax1.grid(True, linewidth=1.5,   which='major', axis='y')

    ax1.set_title(f'DCS Lifetime duration distribution', fontsize=12)

    plt.show()
    # file=f"histo_lifetime_{region}.png"
    # plt.savefig(file, dpi=150, bbox_inches='tight', facecolor='w', transparent=False)
    # fig.clf()
    # plt.close()



# In[8]:


    plt.figure(figsize=(10, 6))
    ecdf = ECDF(Smax)

    # Valeurs à utiliser pour le tracé
    x = np.sort(Smax)
    y = ecdf(x)

    # Création du graphique ECDF
    plt.step(x, y, where='post')

    # Ajout de titres et d'étiquettes d'axes
    plt.title(f'Maximum surface in km² at 235K', fontsize=15)
    plt.xlabel('Smax [km²]',fontsize=12)
    plt.ylabel('Population',fontsize=12)
    plt.xscale('log')
    plt.grid(True, linewidth=0.5, which='minor', axis='x')
    plt.grid(True, linewidth=1,   which='major', axis='x')
    plt.grid(True, linewidth=0.5, which='minor', axis='y')
    plt.grid(True, linewidth=1,   which='major', axis='y')
    plt.show()







# In[20]:


    plt.figure(figsize=(10, 6))  

    plt.plot(data[200].clusters.surfkm2_235K, label='S235K')
    plt.plot(data[200].clusters.surfkm2_220K, label='S220K')
    plt.plot(data[200].clusters.surfkm2_210K, label='S210K')
    plt.plot(data[200].clusters.surfkm2_200K, label='S200K')

    plt.title('Evolution of cold cloud surface of DCS n°'+str(data[200].label), fontsize=15)
    plt.xlabel('Life Cycle [nb steps]',fontsize=12)
    plt.ylabel('cold cloud surface [km²]',fontsize=12)
    plt.legend()

    plt.figure(figsize=(10, 6))  

    plt.plot(data[200].clusters.tb_90thPercentile)
    plt.plot(data[200].clusters.tbavg_235K)

    plt.title('Evolution of 90th percentile of BT for DCS n°'+str(data[200].label), fontsize=15)
    plt.xlabel('Life Cycle [nb steps]',fontsize=12)
    plt.ylabel('Brightness Temperature [K]',fontsize=12)

    plt.show()



# In[21]:


    file_imageTOOCAN = '/home/leo/ISTA/TOOCAN/300K/Dspread1K/irtb/TOOCAN_2.07_MESONH_large300_2D_irtb.nc'

    fh = netCDF4.Dataset(file_imageTOOCAN,'r')
    DCS_number = fh.variables['MCS_label'][:,:,:]

    print('')
    print(fh)
    print(f"DCS_number:  {DCS_number.shape}")
    print(f"DCS_number.max() = {DCS_number.max()}")
    print('')



# In[28]:

    label = np.array([data[iMCS].label for iMCS in np.arange(0,len(data),1)])

    print(f"np.max((label)) = {np.max((label))}")


# In[35]:


    index= np.where(DCS_number == data[200].label)
    print(f"index = {index}")
    print(f"DCS_number[index] = {DCS_number[index]}")


# In[47]:


    WV=np.zeros(np.shape(DCS_number))
    x=500
    y=20
    time=500
    WV[time,y,x]=60


# In[48]:


    print(f"DCS_number[500,x-50:x+50,y-50:+50] = {DCS_number[500,x-50:x+50,y-50:+50]}")

