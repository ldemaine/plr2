import os
import xarray as xr
import sys
try:
    from tqdm.auto import tqdm as tqdm_auto
    from tqdm.contrib.telegram import tqdm as tqdm_tele
    print('    → tqdm is instaled, progressbars are available\n')

except ModuleNotFoundError:
    print('    → tqdm is not instaled, progressbars are inavailable\n')

from .Toocan import load_TOOCAN


#%%=========================================================================%%#
#%% Functions

#%% get_conf

def get_conf(conf_name = '.PLR2.conf', verbose=True ,**kwargs):
    '''
    Read configuration file named conf_name in project and return a dictionary
    Default is ".PLR2.conf" in home directory.

    key word arguments:
    - conf_name ::str  = '.PLR2.conf' (default)
    - verbose   ::bool = True         (default)
    - project   ::str    (default is home directory)
    - path      ::str    (total path of the conf file, if specified overpass
                          everything specified before)
    '''

    project = kwargs.get('project', os.environ['HOME'])
    path    = kwargs.get('path', os.path.join(project,conf_name))

    print( '# Loading configuration file at {:<45} #'.format(path) )

    conf = open(path, 'r')
    temp_list = []
    for line in conf:
        cell = line.strip().replace(" ","").split('=')
        if len(cell)==2:
            temp_list.append(cell)

    dico = dict(temp_list)

    if verbose :
        print('# ',dico)


    return dico



#%%=========================================================================%%#
#%% Class

#%% Sub_dats
class Sub_dats(dict):
    '''
    Fondamental object used for looking for data file in given path.
    Derived from dictionary type.
    Works recursively.

    arguments:
    - path :: posix.DirEntry (the directory)
    - accepted_ext ::list (the extension that the function looks for in dirs)
    '''

    #-------------------------------------------------------------------------#
    def __init__(self,
                 path,
                 file_types = {'nc': ['nc', 'nc4'],
                               'gz': ['gz'],
                               },
                 **kwargs):

        # normal initialization of a dictionary
        dict.__init__(self, **kwargs)

        self.path = path

        self.file_types = file_types

        self.n_types = dict()

        it = 0
        self.Found = False

        scandir_sorted = sorted(
            [entry for entry in os.scandir(path)],
            key = lambda entry: entry.name
        )

        for entry in scandir_sorted:
            if entry.is_dir():
                sd = Sub_dats(entry.path)
                if sd.Found:
                    self.update({entry.name: sd})
                    self.Found = True

            else:
                for t_name, t_ext in file_types.items():
                    if entry.name.split('.')[-1] in t_ext:
                        self.update({it:entry})
                        it        += 1
                        self.Found = True
                        self.n_types[t_name] = self.n_types.get(t_name,0) + 1


        self.n_files = it


    #-------------------------------------------------------------------------#
    def tree(self,
             show_files=True,
             l_indent = [],
             ):
        '''
    Fondamental procedure used to display trees of file structure of data.
    Works recursively.

    arguments:
    - dico ::dict (the dictionnary containing the data structure to be
                  displayed)
    - l_indent ::list = [] (list usefull for recursively know the indentation
                           of the tree)
    - show_files ::bool = True (weither or not the function should display all
    the fiels or only the directories)
        '''

        N = len(self) if show_files else len(self) - self.n_files
        n = 1

        l_indent = l_indent.copy()
        l_indent.append(True)

        for key, value in self.items():

            l_indent[-1] = (n<N)

            indentation = ''
            for BBB in l_indent[:-1]:
                indentation += "│   " if BBB else "    "
            indentation += "├── " if l_indent[-1] else '└── '

            if isinstance(value, dict):
                if (show_files or value.n_files==0):
                    n_files = ''
                else:
                    n_files = '  (files: '
                    for f_key, f_value in value.n_types.items():
                        n_files += f'{f_value} {f_key}'
                    n_files += ')'


                okblue   ='\033[96m'
                stopblue ='\033[0m'

                print( indentation + f"{okblue}{key}{stopblue}{n_files}")

                value.tree(l_indent=l_indent, show_files=show_files)
                n+=1

            elif show_files:
                print( indentation + f"{key} <{value.name}>")
                n+=1


    #-------------------------------------------------------------------------#
    def open_dataset(self, iii, **kwargs):
        return(
            xr.open_dataset(self[iii].path)
        )

    #-------------------------------------------------------------------------#
    def open_toocan(self):
        ds_tooc = self.open_dataset(0)

        gz_tooc = load_TOOCAN(self['FileTracking'][0].path)

        return(ds_tooc, gz_tooc)



#%% Dats

class Dats:
    '''
    Main structure to manage sets of data located in different places.

    Call get_conf function when initialised:
    | Read configuration file named conf_name in project and return a dictionary
    | Default is ".PLR2.conf" in home directory.
    |
    | key word arguments:
    | - conf_name ::str  = '.PLR2.conf' (default)
    | - verbose   ::bool = True         (default)
    | - project   ::str    (default is home directory)
    | - path      ::str    (total path of the conf file, if specified overpass
                           everything specified before)
    '''

    #-------------------------------------------------------------------------#
    def __init__(self, **kwargs):

        self.conf = get_conf(**kwargs)

        for key in self.conf.keys():
            sd = Sub_dats(self.conf[key])
            if sd.Found:
                setattr(self, key, sd)

        print('#\n# Datas structure found:\f')
        self.tree(show_files=False)
        print('\n')


    #-------------------------------------------------------------------------#
    def tree(self, show_files = True):
        dico = self.__dict__.copy()
        del dico['conf']
        # print(dico)
        for key, value in dico.items():
            if isinstance(value, dict):
                print(str(key))
                value.tree(show_files=show_files)


    #-------------------------------------------------------------------------#
    def path(self, List):

        name = List.pop(0)
        value = getattr(self, name)

        while len(List)>0:
            name = List.pop(0)
            value = value[name]


        return(value.path)


#%% outdated, see open functions in Sub_dats
    #-------------------------------------------------------------------------#
    def load_dataset(self, List):
        path = self.path(List)

        return(xr.open_dataset(path))


    #-------------------------------------------------------------------------#
    def load_toocan(self, List):

        ds_tooc = self.load_dataset(List+[0])

        gz_path = self.path(List + ['FileTracking', 0])
        gz_tooc = load_TOOCAN(gz_path)

        return(ds_tooc, gz_tooc)


    #-------------------------------------------------------------------------#
    def load_mnh(self,
                 List,
                 var      = None,
                 verbose  = True,
                 telegram = False,
                 ):

        t_keys = {}
        if telegram:
            from .tele_keys import tele_keys
            t_keys.update(tele_keys)
            tqdm = tqdm_tele
        else:
            tqdm = tqdm_auto

        name = List.pop(0)
        value = getattr(self, name)

        while len(List)>0:
            name = List.pop(0)
            value = value[name]

        L_datasets = []


        ite_load = value.items()
        if verbose and 'tqdm' in sys.modules:
            ite_load = tqdm_auto(ite_load,
                            desc='Loading MNH files',
                            unit='file',
                            )


        for key,ds in ite_load:
            if var==None:
                L_datasets.append(xr.open_dataset(ds.path))
            else:
                L_datasets.append(xr.open_dataset(ds.path)[var])

        L_datasets_sorted = sorted(L_datasets, key= lambda ds: ds.time.data[0])

        ds_mnh = L_datasets_sorted.pop(0)

        if verbose and 'tqdm' in sys.modules:
            L_datasets_sorted = tqdm(L_datasets_sorted,
                                          desc='Concatenation of MNH files',
                                          unit='file',
                                          **t_keys,
                                     )

        for dataset in L_datasets_sorted:
            ds_mnh = xr.concat([ds_mnh, dataset], dim='time')


        return(ds_mnh)

    #-------------------------------------------------------------------------#
