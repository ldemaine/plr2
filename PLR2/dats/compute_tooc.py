import numpy as np
import xarray as xr

import os

#%%

from tqdm.auto             import tqdm as tqdm_auto
# from tqdm.contrib.telegram import tqdm as tqdm_tele


#%%=========================================================================%%#
#%% Classes

class dic_mcs(dict):
    '''
    Dictionary containing informations that has been comupute for each MCS.
    Keys correspond to labels of MCS.

    load_gz, load_perso and save are not be implemented yet.
    '''

    #-------------------------------------------------------------------------#
    def get_full_path(self, **kwargs):

        try:
            save_path = self.save_path

        except:
            try:
                save_path = kwargs['save_path']
            # save_path = kwargs.get('save_path',
            #                        kwargs.get('dats').path(kwargs.get('L')),
            #                        )
            except:
                save_path = kwargs.get('dats').path(kwargs.get('L')),

            self.save_path = save_path

        try:
            title      = self.title

        except:
            title      = kwargs.get('title', 'default')
            self.title = title

        finally:
            full_path  = os.path.join(save_path, title)

        return(full_path)

    #-------------------------------------------------------------------------#
    def save(self, **kwargs):
        '''
        Save this dictionary at L using dats filestructure

        keywword args:
        - save_path ::str
        /or/
        - dats ::dats.Dats
        - L ::list

        '''

        full_path = self.get_full_path(**kwargs)

        os.makedirs(full_path,
                    exist_ok = True,
                    )

        datasets  = [mcs for mcs in self.values()]
        paths     = [f"{full_path}/{label:05}.nc" for label in self.keys()]

        xr.save_mfdataset(datasets, paths, mode='a')

    #-------------------------------------------------------------------------#
    def load_perso(self, **kwargs):

        full_path = self.get_full_path(**kwargs)

        dico = {}

        # for entry in os.scandir(full_path):
        for entry in tqdm_auto(os.scandir(full_path), desc='Loading dic_mcs'):
            ds = xr.open_dataset(entry.path)
            dico.update(
                {int(ds.label) : ds}
            )

        self.merge(dico)

    #-------------------------------------------------------------------------#
    def load_gz(self, ):
        pass

    #-------------------------------------------------------------------------#
    def run_funcs(self, ds_main, ds_tooc, dic_func):
        new_dic = run_funcs(ds_main, ds_tooc, dic_func)

        self.merge(new_dic)

    #-------------------------------------------------------------------------#
    def merge(self, new_dic):
        if len(new_dic) == 0:
            print(f"{new_dic} is empty, there is nothing to do!")

        elif len(self) == 0:
            self.update(new_dic)

        elif self.keys() == new_dic.keys():
            for label, d_mcs in self.items():
                self[label] = xr.merge([self[label], new_dic[label]])

        else:
            print(f"Incompatible labels")

    #-------------------------------------------------------------------------#


#%%=========================================================================%%#
#%% functions


def run_funcs(ds_main, ds_tooc, dic_func):


    N = min(ds_main.time.size, ds_tooc.time.size)
    # N=30

    dico = dic_mcs()


    for ttt in tqdm_auto(range(N), desc = 'Runing funcs over MCSs'):

        main_t     = ds_main.isel(time=ttt).data
        time       = ds_main.isel(time=ttt).time.data
        MCS_labels = ds_tooc.MCS_label.isel(time=ttt).data

        L_labels   = np.unique(MCS_labels[~np.isnan(MCS_labels)])
        masks      = [(MCS_labels==label) for label in L_labels]

        for mask, label in zip(masks, L_labels):
            # value = func(main_t[mask], *args, **kwargs)
            label=int(label)

            values = dict()

            for key, func in dic_func.items():
                value = func(main_t[mask])
                values.update({key: ('time', [value])})

            new_ds = xr.Dataset(
                data_vars = values,
                coords = {'time': [time]},
                attrs = dict(
                    label = label,
                )
            )

            if label in dico:
                dico[label] = xr.concat([dico[label], new_ds], dim='time')
            else:
                dico.update({label: new_ds})

    return(dico)


