# experience_set = {
#     'MNH300:': {'datas'  : ['RCEmip','MNH','300K'],
#                  'toocan' : ['TOOCAN','300K','Dspread1K','irtb'],
#                  }
# }

def load_mnh300(datas, var='rain'):
    ds_tooc, gz_tooc = datas.load_toocan(['TOOCAN','300K','Dspread1K','irtb'])
    ds_mnh = datas.load_mnh(['RCEmip','MNH','300K'],var=var)
    return(ds_tooc, gz_tooc, ds_mnh)

def load_mnh305(datas, var='rain'):
    ds_tooc, gz_tooc = datas.load_toocan(['TOOCAN','305K','Dspread1K','irtb'])
    ds_mnh = datas.load_mnh(['RCEmip','MNH','305K'],var=var)
    return(ds_tooc, gz_tooc, ds_mnh)

