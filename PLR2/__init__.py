import os

###===========================================================###



if __name__== 'PLR2':
    version = 'stable version'
elif __name__=='wd_PLR2':
    version = 'working directory verson'
else:
    version = 'unknow version in {name}'.format(name=__name__)

print('''
                                             
     ▀███▀▀▀██▄▀████▀    ▀███▀▀▀██▄          
       ██   ▀██▄ ██        ██   ▀██▄         
       ██   ▄██  ██        ██   ▄██   ███▀██▄
       ███████   ██        ███████   ███   ██
       ██        ██     ▄  ██  ██▄       ▄▄██
       ██        ██    ▄█  ██   ▀██▄  ▄▄█▀   
     ▄████▄    ██████████ ████▄ ▄███▄▄███████
                                             
    → You are curently on: {user},
    → You are currently using {version}.
'''.format(user= os.uname()[1], version = version)
      )


###===========================================================###

from . import dats, process


