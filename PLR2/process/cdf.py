import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import ecdf


#%%=========================================================================%%#
#%% functions

# def find_labels_centile(datas, centile = 95):
#     '''
#     L_labels = find_labels_centile(datas, centile=95)

#     datas have to be 2D
#     '''

#     value_cent = np.percentile(datas[:,0], centile)

#     L_labels = datas[
#         datas[:,0] > value_cent, 1
#     ]
#     return(L_labels)


#%% plot Cumulative distributive fuction (CDF)

def plot_cdf(datas, ax, title=None, centile=None, median = False):
    '''
    plot_cdf(datas, ax, title=None, centile=95)
    '''

    if datas.ndim == 2:
        datas = datas[:,0]


    d_cdf = ecdf(datas).cdf
    d_min, d_max = datas.min(), datas.max()

    ax.plot(d_cdf.quantiles, d_cdf.probabilities,
            label = 'cdf',
            )


    ax.annotate(f'{d_min}',
                xy             = (d_min,0),
                xytext         = (max(d_min,d_max/25),-0.12),
                arrowprops     = dict(facecolor  = 'black',
                                      width      = 0.1,
                                      headwidth  = 3,
                                      headlength = 5)
                )
    ax.annotate(f'{d_max}',
                xy             = (d_max,1),
                xytext         = (0.9*d_max,0.85),
                arrowprops     = dict(facecolor  = 'black',
                                      width      = 0.1,
                                      headwidth  = 3,
                                      headlength = 5)
                )



    xlim = ax.get_xlim()
    ax.set_xlim(xlim)


    ax.plot(list(xlim), [1,1],
            color = 'black',
            )

    ax.plot(list(xlim), [0,0],
            color = 'black',
            )

    if median:
        d_med = np.nanmedian(datas)

        ax.plot(list(xlim), [0.5, 0.5],
               color     = 'green',
                linestyle = 'dashed',
                label     = 'Median',
                )

        ax.annotate(f'{d_med}',
                    xy         = (d_med,0.5),
                    xytext     = (d_med + d_max/25, 0.35),
                    arrowprops = dict(facecolor  = 'green',
                                      edgecolor  = 'green',
                                      width      = 0.1,
                                      headwidth  = 3,
                                      headlength = 5),
                    color      = 'green',
                    )

    if centile != None:
        d_cent = np.percentile(datas, centile)

        ax.plot(list(xlim), [centile/100, centile/100],
                color     = 'red',
                linestyle = 'dashed',
                label     = f'{centile}th centile',
                )

        ax.annotate(f'{d_cent}',
                    xy         = (d_cent,centile/100),
                    xytext     = (d_cent + d_max/25, centile/100-0.15),
                    arrowprops = dict(facecolor  = 'red',
                                      edgecolor  = 'red',
                                      width      = 0.1,
                                      headwidth  = 3,
                                      headlength = 5),
                    color      = 'red',
                    )

    if title!=None:
        ax.set_title(title)

    ax.grid(True)
    ax.legend(loc='lower right')


#%% plot pdf

def plot_pdf(datas, ax, title=None, bins=500, color='blue'):

    if datas.ndim == 2:
        datas = datas[:,0]


    d_pdf = np.histogram(datas,
                         bins    = bins,
                         density = True,
                         )

    ax.plot((d_pdf[1][:-1] + d_pdf[1][1:])/2, d_pdf[0],
            label = 'PDF',
            color = color,
            )

    ylim = ax.get_ylim()
    ax.set_ylim(ylim)

    d_mean   = np.nanmean(datas)
    d_median = np.nanmedian(datas)
    d_var    = np.nanvar(datas)

    ax.plot([d_mean, d_mean],list(ylim),
            color = 'red',
            label = 'Mean',
            )

    ax.plot([d_median, d_median],list(ylim),
            color = 'green',
            linestyle = 'dashed',
            label = 'Median',
            )

    # ax.plot([d_mean + d_var, d_mean + d_var],list(ylim),
    #         color = 'orange',
    #         linestyle = 'dashed',
    #         label = 'Variance',
    #         )
    # ax.plot([d_mean - d_var, d_mean - d_var],list(ylim),
    #         color = 'orange',
    #         linestyle = 'dashed',
    #         )

    ax.grid(True)
    ax.legend()

#%% plot pdf

def plot_diff_pdf(datas_1, datas_2, ax,
                  title=None,
                  bins=100,
                  name_1='', name_2='',
                  **kwargs,
                  ):

    bins_1 = kwargs.get('bins_1',bins)
    bins_2 = kwargs.get('bins_2',bins)

    if datas_1.ndim == 2:
        datas_1 = datas_1[:,0]

    if datas_2.ndim == 2:
        datas_2 = datas_2[:,0]


    d_pdf_1 = np.histogram(datas_1,
                           bins    = bins_1,
                           density = True,
                           )

    d_pdf_2 = np.histogram(datas_2,
                           bins    = bins_2,
                           density = True,
                           )

    ax.plot((d_pdf_1[1][:-1] + d_pdf_1[1][1:])/2, d_pdf_1[0],
            label = name_1,
            color = 'blue',
            )

    ax.plot((d_pdf_2[1][:-1] + d_pdf_2[1][1:])/2, d_pdf_2[0],
            label = name_2,
            color = 'red',
            )

    ylim = ax.get_ylim()
    ax.set_ylim(ylim)

    d_mean_1   = np.nanmean(datas_1)
    d_median_1 = np.nanmedian(datas_1)
    d_var_1    = np.nanvar(datas_1)

    d_mean_2   = np.nanmean(datas_2)
    d_median_2 = np.nanmedian(datas_2)
    d_var_2    = np.nanvar(datas_2)

    ax.plot([d_mean_1, d_mean_1],list(ylim),
            color = 'blue',
            label = '_Mean',
            )

    ax.plot([d_mean_2, d_mean_2],list(ylim),
            color = 'red',
            label = '_Mean',
            )

    ax.plot([d_median_1, d_median_1],list(ylim),
            color = 'blue',
            linestyle = 'dashed',
            label = '_Median',
            )

    ax.plot([d_median_2, d_median_2],list(ylim),
            color = 'red',
            linestyle = 'dashed',
            label = '_Median',
            )

    ax.grid(True)
    ax.legend()

#%% plot pdf

def plot_diff_cdf(datas_1, datas_2, ax,
                  title=None,
                  name_1='', name_2='',
                  median = True,
                  **kwargs,
                  ):


    if datas_1.ndim == 2:
        datas_1 = datas_1[:,0]

    if datas_2.ndim == 2:
        datas_2 = datas_2[:,0]

    d_cdf_1 = ecdf(datas_1).cdf
    d_cdf_2 = ecdf(datas_2).cdf

    d_min_1, d_max_1 = datas_1.min(), datas_1.max()
    d_min_2, d_max_2 = datas_2.min(), datas_2.max()

    ax.plot(d_cdf_1.quantiles, d_cdf_1.probabilities,
            label = name_1,
            color = 'blue',
            )

    ax.plot(d_cdf_2.quantiles, d_cdf_2.probabilities,
            label = name_2,
            color = 'red',
            )

    xlim = ax.get_xlim()
    ax.set_xlim(xlim)

    ax.plot(list(xlim), [1,1],
            color = 'black',
            )

    ax.plot(list(xlim), [0,0],
            color = 'black',
            )

    if median:
        d_med_1 = np.nanmedian(datas_1)
        d_med_2 = np.nanmedian(datas_2)

        ax.plot(list(xlim), [0.5, 0.5],
                color     = 'green',
                linestyle = 'dashed',
                label     = 'Median',
                )

        ax.annotate(f'{d_med_1}',
                    xy         = (d_med_1,0.5),
                    xytext     = (d_med_1 + d_max_1/25, 0.35),
                    arrowprops = dict(facecolor  = 'blue',
                                      edgecolor  = 'blue',
                                      width      = 0.1,
                                      headwidth  = 3,
                                      headlength = 5),
                    color      = 'blue',
                    )

        ax.annotate(f'{d_med_2}',
                    xy         = (d_med_2,0.5),
                    xytext     = (d_med_2 + d_max_2/25, 0.45),
                    arrowprops = dict(facecolor  = 'red',
                                      edgecolor  = 'red',
                                      width      = 0.1,
                                      headwidth  = 3,
                                      headlength = 5),
                    color      = 'red',
                    )





#%% plot diff quant (inv cdf)

def plot_diff_quant(datas_1, datas_2, ax,
                  N = 500,
                  title=None,
                  **kwargs,
                  ):

    if datas_1.ndim == 2:
        datas_1 = datas_1[:,0]

    if datas_2.ndim == 2:
        datas_2 = datas_2[:,0]

    d_cdf_1 = ecdf(datas_1).cdf
    d_cdf_2 = ecdf(datas_2).cdf

    probas = np.linspace(0,1,N)
    d_diff_P = np.zeros(N)

    for i in range(N):
        p = probas[i]

        i_1 = np.searchsorted(d_cdf_1.probabilities, p)
        i_2 = np.searchsorted(d_cdf_2.probabilities, p)

        v_1 = d_cdf_1.quantiles[i_1]
        v_2 = d_cdf_2.quantiles[i_2]

        d_diff_P[i] = 100 * (v_2 - v_1)/(5*v_1)


    ax.plot(probas, d_diff_P,
             color = 'green',
             )

    xlim = ax.get_xlim()
    ax.set_xlim(xlim)

    ax.plot(list(xlim), [0,0],
             color = 'black',
             linestyle = 'dashed',
             )

#%%=========================================================================%%#
#%% Some use example!
if __name__=='__main__':
# if True:

    ### initialisation of datasets

    from ..dats import Dats
    dats = Dats()

    ds_tooc, gz_tooc = dats.load_toocan(['TOOCAN','300K','Dspread1K','irtb'])

    ### drawing precipitation using matplotlib only

    L_pr_tot = np.array([
        [mcs.pr_total, mcs.label] for mcs in gz_tooc
    ])

    L_pr_max = np.array([
        mcs.pr_max for mcs in gz_tooc
    ])



    fig = plt.figure(#figsize = (),
        layout = 'constrained',
    )

    axs = fig.subplots(2,
                       sharex = True,
                       )

    plot_pdf(L_pr_tot, axs[0])

    plot_cdf(L_pr_tot, axs[1], median=True, centile=95)

    fig.suptitle('Pr total')

    plt.savefig('first_cdf.pdf')
    plt.savefig('first_cdf.jpg')
    plt.show()




    ###

    pr_95 = np.percentile(L_pr_tot[:,0], 95)

    L_label_centile = L_pr_tot[L_pr_tot[:,0]>pr_95,1].astype(int)



    ###

    L_Amax = np.array([
        [mcs.surfmaxkm2_235K, mcs.label] for mcs in gz_tooc
    ])

    L_Acum = np.array([
        [mcs.surfcumkm2_235K, mcs.label] for mcs in gz_tooc
    ])

    L_duration = np.array([
        [mcs.duration, mcs.label] for mcs in gz_tooc
    ])

    L_tmax = np.array([
        [mcs.Tmax, mcs.label] for mcs in gz_tooc
    ])

    ##-----------------------------------------##

    fig = plt.figure(figsize = (7,10),
                     layout = 'constrained',
                     )

    axs = fig.subplots(3,)


    plot_cdf(L_Amax, axs[0], title='Maximum Area')
    plot_cdf(L_Acum, axs[1], title='Cumulated Area')
    plot_cdf(L_duration, axs[2], title='Duration')

    plt.savefig('Amax_Acum_duration-cdf.jpg')
    plt.savefig('Amax_Acum_duration-cdf.pdf')
    plt.show()





