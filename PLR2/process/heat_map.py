import matplotlib.pyplot as plt
import numpy as np


#%%=========================================================================%%#
#%% FUNCTIONS

#%% heatmap

def heatmap(X, Y, Z = None, /,
            bins    = 100,
            **kwargs,
            ):

    n_bins_X = kwargs.get('bins_X',bins)
    n_bins_Y = kwargs.get('bins_Y',bins)

    if X.ndim == 2:
        X = X[:,0]

    if Y.ndim == 2:
        Y = Y[:,0]

    if Z is not None and Z.ndim == 2:
        Z = Z[:,0]

    N = len(X)

    X_bins = np.linspace(X.min(), X.max(), n_bins_X)
    Y_bins = np.linspace(Y.min(), Y.max(), n_bins_Y)

    density = np.zeros((n_bins_Y, n_bins_X))
    heat_Z  = density.copy()

    X_ind = np.searchsorted(X_bins, X)
    Y_ind = np.searchsorted(Y_bins, Y)

    for i in range(N):
        density[Y_ind[i], X_ind[i]] += 1
        if Z is not None:
            heat_Z[Y_ind[i], X_ind[i]] += Z[i]

    heat_Z = heat_Z/density if Z is not None else None
    density = density/N

    return(X_bins, Y_bins, density, heat_Z)

